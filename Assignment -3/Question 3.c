#include<stdio.h>
#include<stdlib.h>

int main()
{
  int n, r = 0;

  printf("Enter a number :\n");
  scanf("%d", &n);

    while (n > 0)
    {
      r = r * 10;
      r = r + n%10; // or we can use r= r*10 + n%10 . Loop repeats till n=0.
      n = n/10;
    }

  printf("Reverse of the number = %d\n", r);

  return 0;
}
