#include<stdio.h>
#include<stdlib.h>
int main()
{
    const double pi = 3.14;
    float h,r,v;

    printf("Enter height and radius :\n");
    scanf("%f %f",&h,&r);

    v = pi*r*r*h/3;

    printf("Volume of the cone = %.3f",v);

    return 0;
}
